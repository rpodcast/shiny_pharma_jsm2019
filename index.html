<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <title>Developing Powerful Shiny Applications for Life Sciences</title>
    <meta charset="utf-8" />
    <meta name="author" content="Eric Nantz   Eli Lilly and Company   Joint Statistical Meetings | 29 July 2019   Slides: rpodcast.gitlab.io/shiny_pharma_jsm2019    @thercast    rpodcast    r-podcast.org" />
    <link href="libs/remark-css/default.css" rel="stylesheet" />
    <link href="libs/remark-css/metropolis.css" rel="stylesheet" />
    <link href="libs/remark-css/metropolis-fonts.css" rel="stylesheet" />
    <link href="libs/font-awesome/css/fontawesome-all.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="custom_css/title.css" type="text/css" />
    <link rel="stylesheet" href="custom_css/footer.css" type="text/css" />
    <link rel="stylesheet" href="custom_css/two_columns.css" type="text/css" />
    <link rel="stylesheet" href="custom_css/fontsize.css" type="text/css" />
    <link rel="stylesheet" href="custom_css/misc.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Developing Powerful Shiny Applications for Life Sciences
## Best Practices and Lessons Learned
### Eric Nantz <br> Eli Lilly and Company <br> Joint Statistical Meetings | 29 July 2019 <br> Slides: <a href='http://rpodcast.gitlab.io/shiny_pharma_jsm2019'>rpodcast.gitlab.io/shiny_pharma_jsm2019</a> <br> <a href='https://twitter.com/thercast'> <i class='fab  fa-twitter '></i><span class="citation">@thercast</span></a> <br> <a href='https://github.com/rpodcast'> <i class='fab  fa-github '></i>rpodcast</a> <br> <i class='fas  fa-microphone '></i> <a href='https://r-podcast.org'>r-podcast.org</a><br>

---




layout: true

&lt;div class="my-footer"&gt;&lt;span&gt;JSM 2019&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;Developing Shiny Applications for Life Sciences&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;&amp;emsp;rpodcast.gitlab.io/shiny_pharma_jsm2019&lt;/span&gt;&lt;/div&gt; 

---
class: fullscreen, inverse, top, center, text-black

background-image: url(img/long_road.jpg)

.font250[**My Journey with Shiny**]

---

# 2012 &amp; 2013...

* [`shiny`](http://shiny.rstudio.com/) released to CRAN and I became an early adopter
* RStudio releases open-source __shiny server__ available for linux
* Deployed on my home server and instantly hooked!

--

😲 This could be a game-changer in our line of work ...

--

![:scale 30%](img/hat_rlogo.png)
--
![:scale 30%](img/hat_tux.png)
--
![:scale 30%](img/monitor_jericho.png)
--

Recommended Reading: [Analytics Administration for R](https://rviews.rstudio.com/2017/06/21/analytics-administration-for-r/) by Nathan Stephens

---

# Shiny catches on ....

.pull-left[
.font150[
* Multiple statisticians &amp; scientists begin creating Shiny applications
* Difficult to keep up with the demands!
     + Licensed RStudio Server Professional and __Shiny Server Professional__
* Organized an internal __R &amp; Shiny Day__ in partnership with RStudio
]
]

.pull-right[
![](https://media.giphy.com/media/xT5LMqPBclx4HKdZXG/giphy.gif)
]

---

class: inverse, clear, center, middle

# Practical Advice

---
background-image: url(img/with-great-power-comes-great-responsibility.jpg)
background-size: contain
class: center, clear

---

# Application as a Package

.pull-left[

## `golem`: create a robust <i class="fab  fa-r-project " style="color:#384CB7;"></i> __package__ infrastructure for your Shiny app

* Scripts guide you with first steps and convenience functions
* Encourages testing and development best practices
* Easily sets up your app for deployment on a variety of platforms

]

.pull-right[
.center[
&lt;img src="https://raw.githubusercontent.com/ThinkR-open/golem/master/inst/rstudio/templates/project/golem.png" width=300px&gt;
&lt;br&gt;
[UserR! 2019 presentation](https://github.com/VincentGuyader/user2019/raw/master/golem_Vincent_Guyader_USER!2019.pdf)
&lt;br&gt;
[thinkr-open.github.io/golem/](https://thinkr-open.github.io/golem)
&lt;br&gt;
[Shiny Developer Series Episode 2](https://shinydevseries.com/post/episode-2-golem/)
]
]

---

# Application Architecture

For applications big or small, I highly recommend using [__modules__](http://shiny.rstudio.com/articles/modules.html):
--

- Set of R functions optimized for `shiny` applications, typically composed of user-interface and server-side logic
- Avoid namespace collisions when using same widget across different parts of your app
- Allows you to  __compartmentalize__ distinct app components
- 🏗 Passing values back and forth can be tricky!
- Further reading: Article on [communication between modules](http://shiny.rstudio.com/articles/communicate-bet-modules.html)

--

.pull-left[
.code80[
```r
schemeSelectUI &lt;- function(id) {
  # mandatory call to set up unique namespace
  ns &lt;- NS(id)
  uiOutput(ns("select_scheme_render"))
}

# within application UI
schemeSelectUI(ns("first_select"))

# within application server-side processing
my_scheme &lt;- callModule(schemeSelect, 
                        "first_select", 
                        scheme_choices)
```
]
]

.pull-right[
.code50[
```r
schemeSelect &lt;- function(input, output, session, 
                         scheme_choices, label = "Scheme") {
  
  # render the select input based on the available schemes
  output$select_scheme_render &lt;- renderUI({
    ns &lt;- session$ns
    selectInput(
      inputId = ns("select_scheme"),
      label = label,
      choices = scheme_choices(),
      selected = scheme_choices()[1],
      selectize = FALSE
    )
  })
  
  outputOptions(output,
                "select_scheme_render", 
                suspendWhenHidden=FALSE)
  # add more related objects if needed
  reactive({ input$select_scheme})
}
```
]
]

---

background-image: url(img/picard_ui_meme.jpg)
background-size: 800px
background-position: 50% 50%

# Enhancing your user interfaces

---

# Enhancing your user interfaces

Investing time to prevent cryptic errors based on user interactions will make your development __much easier__!

--

&lt;center&gt;![:scale 20%](img/shinyjs-logo-whitebg-small.png)&lt;/center&gt;

* Created by [Dean Attali](https://deanattali.com/)
* Wraps powerful javascript utilities in easy-to-use functions
    + Toggle display of user interfaces elements 
    + Disable and enable buttons or other inputs
    + Easily bind custom javascript code in your application

.left-column-med[
.code70[
```r
# somewhere in app UI
actionButton(ns("activate_session"), 
             "Activate Session", 
             class = "btn-primary")
```
]
]

.right-column-med[
.code70[
```r
# somewhere in server logic
observe({
  cond &lt;- length(input$mytable_rows_selected) != 0
  shinyjs::toggleState(
    id = "activate_session",
    condition = cond)
})
```
]
]

---

# Enhancing your user interfaces

Investing time to prevent cryptic errors based on user interactions will make your development __much easier__!

[__shinyFeedback__](https://github.com/merlinoa/shinyFeedback): Displaying user feedback next to Shiny inputs

![](img/shinyfeedback.gif)

.left-column-med[
.code70[
```r
# ui element
textInput(
  inputId = "indication",
  label = "Indication",
  value = ""
)
```
]
]

.right-column-med[
.code75[
```r
# server-side processing
observeEvent(input$indication, {
  shinyFeedback::feedbackWarning(
    inputId = "indication",
    condition = stringr::str_length(input$indication) &gt; 18,
    text = "Slide title might be split to multiple lines, 
            consider abbreviating"
  )
})
```
]
]

---

# New interfaces with HTML templates

### A growing ecosystem of packages for new user interfaces on top of Shiny

* [`bs4Dash`](https://rinterface.github.io/bs4Dash/): Bootstrap 4 shinydashboard
* [`shinydashboardPlus`](https://rinterface.github.io/shinydashboardPlus/): Extensions for shinydashboard
* [`shinyF7`](https://rinterface.github.io/shinyF7/): shiny API for Framework7 (iOS and Android)

Draw inspiration from the entries to the first-ever [Shiny Contest](https://blog.rstudio.com/2019/04/05/first-shiny-contest-winners/)!

.pull-left[
.center[
![:scale 80%](img/2019-04-05-virtual-patient-simulator.gif)
.font80[
[Virtual Patient Simulator](https://community.rstudio.com/t/shiny-contest-submission-a-virtual-lab-for-teaching-physiology/25348)
]
]
]

.pull-right[
.center[
![:scale 90%](img/2019-04-05-shinylego.gif)
.font80[
[Shiny LEGO Mosaic Maker](https://community.rstudio.com/t/shiny-contest-submission-the-shiny-lego-mosaic-creator/25648)
]
]
]

---

class: inverse, center, middle, clear

# Reproducibility &amp; Automation

---

# Personalized Experience

Large-scale applications used extensively in __iterative__ workflows

--

How can we let the user resume their work and initialize multiple workflows?

--

* `shiny` now supports [__bookmarkable state__](http://shiny.rstudio.com/articles/bookmarking-state.html) (URL or server-side)
    + Useful for many workflows, but I envisioned more than just state of application

--

💡 Shiny Server Pro &amp; RStudio Connect: Application processes owned by the __logged-in user__!

.pull-left[

```r
user &lt;- reactive({
  if(!is.null(session$user))  {
*   my_user &lt;- session$user
  } else {
    my_user &lt;- Sys.getenv("USER")
  }
  return(my_user)
})
```
]

.pull-right[
![:scale 90%](https://media.giphy.com/media/50OAJNulFBBrq/source.gif)
]


---

# Application Workflows

![:scale 80%](img/session_save_screenshot.png)

--

* Create an overall application session directory: `/home/&lt;logged_in_user&gt;/.myapp_files`
* Sync [`packrat`](https://rstudio.github.io/packrat/) application library, enabling a __reproducible__ <i class="fab  fa-r-project " style="color:#384CB7;"></i> execution environment
* Allow user to create multiple __projects__ for separation of workflows

--

.left-column-small[
.code60[
```
/home/bob/.myapp_files
-- hpc_workdir
   |__packrat
-- mysession
*   |__object_repo
*   |__settings.RData
-- sessions.sqlite3
```
]
.code60[

]
]

.right-column-big[
.font90[
📒 Inspired by the [Radiant](https://radiant-rstats.github.io/docs/) Shiny app created by Vincent Nijs

💡 Utilize packages like [`storr`](http://richfitz.github.io/storr/) to efficiently cache R objects to disk

]
]

---

# Offload Processing

.pull-left[
.font90[
When possible: Send CPU-intensive and __High Throughput Computing__ (HTC) workflows to a separate computing infrastructure

* Less burden on the server 
* User can safely interact with other parts of application while jobs are processing

Possible <i class="fab  fa-r-project " style="color:#384CB7;"></i> based solutions:

* [`batchtools`](https://mllg.github.io/batchtools/): Wraps HPC job submissions for variety of systems (Slurm, SGE, OpenLava, TORQUE/OpenPBS, LSF, Docker Swarm)
* [`future.batchtools`](https://github.com/HenrikBengtsson/future.batchtools): utilizes the [`future`](https://github.com/HenrikBengtsson/future) <i class="fab  fa-r-project " style="color:#384CB7;"></i> package API for parallel and distributed processing via `batchtools`
* [`drake`](https://github.com/ropensci/drake): <i class="fab  fa-r-project " style="color:#384CB7;"></i>-focused pipeline toolkit for reproducibility and HPC (Will Landau)
* [`clustermq`](https://github.com/mschubert/clustermq): Utilizes `ZeroMQ` to send jobs to various HPC systems
]
]

.pull-right[
![:scale 95%](img/hpc_cluster.jpg)

.font80[
__Incorporating within `shiny`__:

* [`reactiveValues`](http://shiny.rstudio.com/reference/shiny/1.0.5/reactiveValues.html) to track progress and key events
* Inform user via [modals](http://shiny.rstudio.com/articles/modal-dialogs.html) or the new [`shinyalert`](https://daattali.com/shiny/shinyalert-demo/) package
* Cache _integrated_ results to disk
]
]

---

# Launching Simulations

![:scale 60%](img/launch_sim_table.png)
--

.code60[

```r
observeEvent(input$launchSims, {
  # perform sanity checks of the user's settings
  check_the_settings()
  
  # launch simulations
  launch_sims(sessionDir = parent_dir, simID = hpcSimSelect(), n_batch = n_batch)
  
  # answer questions at this stage in process
* any_sim_launched(TRUE)
* which_sim_running(hpcSimSelect())
* total_jobs(n_batch)
* total_sims(n_sims)
* all_complete(FALSE)
})
```
]


---

# Monitoring _practically_ completed jobs

.pull-left[
.code60[

```r
n_complete &lt;- reactive({
    
  # Re-execute after 5 seconds
* invalidateLater(5000, session)
  
  # set default of 0 since nothing run yet
  n_completed_default &lt;- 0
  
  # perform checks only if sims launched
  if (any_sim_launched()) {
    
    # perform custom processing on results files
    n_completed_df &lt;- get_n_completed(log_path)
    
    # if no sims have completed in the 
    # launched project, then simply return 0
    if (nrow(n_completed_df) &lt; 1) {
      return(n_completed_default)
    } else {
      # update reactiveVal for sims_complete
*     sims_complete(nrow(n_completed))
      return(nrow(n_completed))
    }
  } else {
    return(n_completed_default)
  }
})
```

]
]

.pull-right[
Each job writes results to a file

Once this file is available, I consider the job _practically_ completed

`get_n_completed()` searches for all results files and returns data frame with status

💡 Even a typical `reactive` can be __invalidated__ at schedule intervals!
]

---

# Reproducibility 

Allowing user to download an __automated__ report with key results important for documentation and reproducibility

__Breaking News__: The new [`shinymeta`](https://rstudio.github.io/shinymeta/) package allows you to capture logic in a Shiny app and expose it as code for running outside the app!

--

.pull-left[
.code70[

```r
# within download handler processing
# run knit_expand on reports
report_dir &lt;- "templates"
report_files &lt;- c("report1.Rmd", "report2.Rmd")

filled_text &lt;- rep(NA, length(report_files))

for (i in seq_along(report_files)) {
  filled_text[i] &lt;- knitr::knit_expand(
    file = file.path(report_dir, report_files[i]), 
*   user = user(),
*   sessionid = sessionid(),
*   sim_id = results_select()
  )
}

cat(filled_text, file = file)
```
]
]

.pull-right[
.font80[
Template `report1.Rmd` YAML header:
]

.code80[
```
---
title: "Summary Report"
output: html_document
params:
  user: "{{user}}" 
  sessionid: "{{sessionid}}" 
  sim_id: "{{sim_id}}" 
---
```
]
]

---

background-image: url(img/end_is_near.png)
background-size: 800px
background-position: 50% 50%

# Wrapping Up

---

# Parting Thoughts

.font110[
Shiny enables interactive analyses, reproducibility, and automation in prototyping __and__ production workflows
]

--

.font110[
* 🛡 Ecosystem of packages available to __defend__ user interfaces from potential mishaps
* 📦 Organizing souce code via __modules__ improves collaboration and organization
* 🗒 Ability to customize workflows tailored to iterative analyses, keeping __reproducibility__ front and center
* 💻 Integration with external computing infrastructures to offload parallel analyses and obtain results faster
]

--

.font110[
Additional Advice:

* Use version control (i.e. `git` and GitHub/GitLab) for effective development, especially in team environments
* Share prototypes with your intended audience to get feedback __early__ and __often!__
* Leverage the new [`shinytest`](https://rstudio.github.io/shinytest/articles/shinytest.html) package for automated testing
* Have a robust design in mind for large-scale applications
]

---

# Thank you!

.center[
&lt;table style="border-style:none;padding-top:30px;" class=".table"&gt;
  &lt;br /&gt;
  &lt;br /&gt;
  &lt;tr&gt;
    &lt;th style="padding-right:25px!important" align="center"&gt;&lt;a href="https://twitter.com/thercast"&gt; &lt;i class="fab fa-twitter fa-3x"&gt;&lt;/i&gt; &lt;/a&gt;&lt;/th&gt;
    &lt;th style="padding-left:25px!important" align="center"&gt;&lt;a href="https://github.com/rpodcast"&gt; &lt;i class="fab fa-github fa-3x"&gt;&lt;/i&gt; &lt;/a&gt;&lt;/th&gt;
    &lt;th style="padding-left:25px!important" align="center"&gt;&lt;a href="https://gitlab.com/rpodcast"&gt; &lt;i class="fab fa-gitlab fa-3x"&gt;&lt;/i&gt; &lt;/a&gt;&lt;/th&gt;
    &lt;th style="padding-left:25px!important" align="center"&gt;&lt;a href="https://r-podcast.org"&gt; &lt;i class="fa fa-microphone fa-3x"&gt;&lt;/i&gt; &lt;/a&gt;&lt;/th&gt;
  &lt;/tr&gt;
  &lt;tr style="background-color:#fafafa"&gt;
    &lt;th style="padding-right:25px!important"&gt;&lt;a href="https://twitter.com/thercast"&gt; @thercast &lt;/a&gt;&lt;/th&gt;
    &lt;th style="padding-left:25px!important"&gt;&lt;a href="https://github.com/rpodcast"&gt; @rpodcast &lt;/a&gt;&lt;/th&gt;
    &lt;th style="padding-left:25px!important"&gt;&lt;a href="https://gitlab.com/rpodcast"&gt; @rpodcast &lt;/a&gt;&lt;/th&gt;
    &lt;th style="padding-left:25px!important"&gt;&lt;a href="https://r-podcast.org"&gt; r-podcast.org &lt;/a&gt;&lt;/th&gt;
  &lt;/tr&gt;&lt;/table&gt;
]

.font110[
Other efforts in the <i class="fab  fa-r-project " style="color:#384CB7;"></i> community:
* Shiny Developer Series in partnership with RStudio: [shinydevseries.com](https://shinydevseries.com)
* Curator for  [R Weekly](https://rweekly.org/)
* [RStudio Community](https://community.rstudio.com/) sustainer
* Member of [Rbind](https://support.rbind.io/) administrator team

Slides created with the [xaringan](https://slides.yihui.name/xaringan) package: [rpodcast.gitlab.io/shiny_pharma_jsm2019](https://rpodcast.gitlab.io/shiny_pharma_jsm2019)
]
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script src="macros.js"></script>
<script>var slideshow = remark.create({
"ratio": "16:9",
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false,
"selfContained": true
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();</script>

<script>
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
